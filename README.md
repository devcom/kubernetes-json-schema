# kubernetes-json-schema

This project adds some additional [JSON Schema](https://json-schema.org/)
definitions for validating Kubernetes manifests. This can be used
by something such as [Kubeconform](https://github.com/yannh/kubeconform)
as part of your gitlab-ci pipeline.

## Gitlab-CI

To use these schemas in kubeconform, use the `-schema-location` parameter:

```
stages:
  - validate

lint-kubeconform:
  stage: validate
  image:
    name: ghcr.io/yannh/kubeconform:latest-alpine
    entrypoint: [""]
  script:
    - |
      /kubeconform  \
        -schema-location default \
        -schema-location https://code.vt.edu/devcom/kubernetes-json-schema/-/raw/main/{{.ResourceKind}}.json \
        -ignore-filename-pattern .gitlab-ci.yml \
        "${CI_PROJECT_DIR}"
```

